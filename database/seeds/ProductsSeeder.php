<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    public function run()
    {
        $result =
        [
            [
                'name' => 'Чашка',
                'slug' => 'chashka',
                'description' => 'Описание продукта',
                'img' => 'chashka.jpg',
                'price' => 9999,
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Стол',
                'slug' => 'stol',
                'description' => 'Описание продукта',
                'img' => 'stol.jpg',
                'price' => 9999,
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Диван',
                'slug' => 'divan',
                'description' => 'Описание продукта',
                'img' => 'divan.jpg',
                'price' => 9999,
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Шкаф',
                'slug' => 'shkath',
                'description' => 'Описание продукта',
                'img' => 'shkath.jpg',
                'price' => 9999,
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Дверь',
                'slug' => 'dver',
                'description' => 'Описание продукта',
                'img' => 'dver.jpg',
                'price' => 9999,
                'created_at' => new DateTime,
                'updated_at' => null
            ],
            [
                'name' => 'Окно',
                'slug' => 'okno',
                'description' => 'Описание продукта',
                'img' => 'okno.jpg',
                'price' => 9999,
                'created_at' => new DateTime,
                'updated_at' => null
            ]
        ];

        //Умножение фейковых товаров
        $products = $result;
        for ($i = 1; $i < 10; $i++)
        { 
            foreach ($result as $key => $value)
            {
                if ($i == 1)
                {
                    $name[$key] = $value['name'];
                    $slug[$key] = $value['slug'];
                }
                $result[$key]['name'] = $name[$key] . ' ' . $i;
                $result[$key]['slug'] = $slug[$key] . '_' . $i;
            }
            $products = array_merge($products, $result);
        }

        DB::table('products')->insert($products);
    }
    
}