<?php
use Illuminate\Support\Facades\Route;

Route::post('/sendtelegram', 'Ajax\TelegramController@send');
Route::get('{any}', function (){ return view('main');})->where('any', '.*');