import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import PageNotFound from '../view/PageNotFound'
import HomeView from '../view/HomeView'
import CatalogView from '../view/CatalogView'
import BacketView from '../view/BacketView'

const router = new VueRouter
(
    {
        mode: 'history',
        routes: [
            {
                path: "*",
                name: 'error',
                component: PageNotFound
            },
            {
                path: '/',
                name: 'home',
                component: HomeView
            },
            {
                path: '/catalog',
                name: 'catalog',
                component: CatalogView
            },
            {
                path: '/sopping-cart',
                name: 'backet',
                component: BacketView
            }
        ],
        linkActiveClass: 'active_link'

    }
)

export default router