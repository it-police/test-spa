require('./bootstrap')

import Vue from 'vue'
import Main from './components/MainComponent'
import router from './router/main'
import store from './store/main'

const app = new Vue
(
    {
        el: '#spa', //обозначаем контейнер
        render: h => h(Main), //Обозначаем главный компонент по умолчанию
        store, //Так как синтаксис ES6+ можно указывать значение свойства с одинаковым названием без указания значения store: store === store
        router //Обозначаем роутер
    }
)
