import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from "vuex-persistedstate";

//Модули
import product from './modules/product'
import basket from './modules/backet'

Vue.use(Vuex)

export default new Vuex.Store
(
    {
        modules:
        {
            product,
            basket
        },
        plugins: [createPersistedState()]

    }
)