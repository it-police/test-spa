export default
{
    actions:
    {
        requestProducts(context, urlPage)
        {
            urlPage = urlPage || 'api/product'
            context.commit('replaceContentToLoader')
            axios
                .get(urlPage)
                .then(response => {
                    context.commit('updateProducts', response)
                })
                .catch(error => {
                    console.log('Ошибка', error)
                })
        }
    },
    //=====
    mutations:
    {
        updateProducts(state, data)
        {
            state.productsData = {
                loader: false,
                data: data.data,
            }
        },
        replaceContentToLoader(state){
            state.productsData.loader = true
        }
    },
    //=====
    state:
    {
        productsData: {
            loader: true,
            data: []
        }
    },
    //=====
    getters:
    {
        products(state)
        {
            return state.productsData
        }
    }
}
