const reducer = (previousValue, currentValue) => previousValue + currentValue;

export default  {
    actions:
    {
        sortBacket(context, product)
        {
            let object = {'count': 1, ...product};
            context.commit('updateBasket', object)
        },
        removeToBasket(context, id){
            context.commit('removeBasket', id)
        },
        sendMessage(context, id){
            context.commit('sendMessage')
        }
    },
    //=====
    mutations:
    {
        updateBasket(state, data){
            if (state.basketData.products.length)
            {
                let existProduct = true
                state.basketData.products.map
                (
                    item =>
                    {
                        if (item.id === data.id)
                        {
                            existProduct = false
                            item.count++
                            state.basketData.count++
                        }
                    }
                )
                if (existProduct)
                {
                    state.basketData.count++
                }
            }
            else
            {
                state.basketData.products.push(data)
                state.basketData.count++
            }
            state.basketData.total = state.basketData.products.map(i => i.price).reduce(reducer);
        },
        removeBasket(state, id)
        {
            state.basketData.products.map
            (
                (item, index) =>
                {
                    if (item.id === id)
                    {
                        if (item.count > 1) item.count--
                        else state.basketData.products.splice(index, 1)
                    }
                }
            )
            state.basketData.count--
        },
        sendMessage(state){
            let data = JSON.stringify(state.basketData)
            axios.post("/sendtelegram", {data: data})
             .then(function (response) {
                state.basketData.products = []
                state.basketData.count = 0
             })
             .catch(function (error) {
               console.log(error);
             });
        }
    },
    //=====
    state:
    {
        basketData: {
            products: [],
            count: 0,
            total: 0
        }
    },
    //=====
    getters:
    {
        basket(state)
        {
            return state.basketData
        }
    }
}
